﻿using System;
using System.Linq;

namespace Lab6
{
    public struct Id
    {
        public string type, color;
        public int wheels, count, weight, price;

        public Id(string a1, string a2, int a3, int a4, int a5, int a6)
        {
            type = a1;
            color = a2;
            wheels = a3;
            count = a4;
            weight = a5;
            price = a6;
        }
    };

    class MainClass
    {
        static void Main()
        {
            Id[] car = new Id[5];
            car[0] = new Id("Sedan", "blue", 4, 4, 1000 , 7000);
            car[1] = new Id("Mini-van", "white", 8, 4, 4000, 10000);
            car[2] = new Id("Universal", "red", 4, 4, 1200 , 4500);

            double maxPageCount = 0;
            int maxIndex = 0;

            for (int i = 0; i < 3; i++)
            {
                if (car[i].price > maxPageCount)
                {
                    maxPageCount = car[i].price;
                    maxIndex = i;
                }
            }

            Console.WriteLine("The car with the highest price is {0}, Its pirce is {1} $$.", car[maxIndex].type, car[maxIndex].price);
            Console.ReadKey();
        }
    }
}