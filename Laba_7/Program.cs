﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication95
{
    class Program
    {
        public static double A(double x)
        {
            return Math.Sin(2*x) + 5;
        }

        public static double B(double x)
        {
            return Math.Cos(Math.Pow(x,3));
        }

        public static double C(double x)
        {
            return Math.Pow(x, 1/3) + Math.Tan(x);
        }

        public static double H(double a, double b, double c)
        {
            return (3*a*b) - 4;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Вывод: ");
            double x = Convert.ToDouble(Console.ReadLine());
            double a = A(x);
            double b = B(x);
            double c = C(x);
            double h = H(a, b, c);
            Console.WriteLine("H() = {0}", h);
            Console.ReadLine();

        }
    }
}