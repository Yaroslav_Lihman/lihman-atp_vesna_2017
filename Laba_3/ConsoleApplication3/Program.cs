﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba_3
{

	class MainClass
	{
		public static void Main(string[] args)
		{
			string s = Console.ReadLine();
			double z = Convert.ToDouble(s);
			if (z < 0)
				z = Math.Pow(z, 3) + Math.Pow(Math.E, z);
			else if (z >= 0 && z <= 8)
				z = Math.Pow(Math.Cos(z), 4) + 3 / Math.Pow(z, 3);
			else
				z = Math.Tan(z + (1 / z));
			double x = Math.Log(z - Math.Pow(Math.E, z * 2));
			Console.WriteLine("Otvet = " + x);
			Console.ReadKey();

		}
	}
}
